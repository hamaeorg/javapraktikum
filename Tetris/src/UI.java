import javafx.animation.PathTransition;
import javafx.animation.Transition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class UI extends Application {

	public static void lauch(String[] args){
		launch(args);	
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Pane stackpane = new Pane();
		Rectangle rectangle = new Rectangle(100, 200, 30, 30);
		rectangle.setFill(Color.BLUE);
		Rectangle rectangle2 = new Rectangle(100, 231, 30, 30);
		rectangle2.setFill(Color.BLUE);
		Rectangle rectangle3 = new Rectangle(131, 200, 30, 30);
		rectangle3.setFill(Color.BLUE);
		stackpane.getChildren().addAll(rectangle, rectangle2, rectangle3);
		
		Scene scene = new Scene(stackpane, 500, 500);
		primaryStage.setTitle("Teteris");
		primaryStage.setScene(scene);
		primaryStage.show();		
		
		scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
		      if(key.getCode()==KeyCode.ENTER) {
		          System.out.println("vajutasid enterit");
		          //rectangle.setRotate(rectangle.getRotate() + 90);
		          rectangle.setLayoutY(rectangle.getLayoutY() + 10);
		          rectangle2.setLayoutY(rectangle2.getLayoutY() + 10);
		          rectangle3.setLayoutY(rectangle3.getLayoutY() + 10);
		      }
		});
	}	
}
