package praktikum04;

import lib.TextIO;

public class CumLaudeDetektor {
	public static void main(String[] args) {

		int KaalutudKeskhinne;
		int L6put22;

		System.out.print("Sisesta kaalutud keskhinde ");
		while (true) {
			KaalutudKeskhinne = TextIO.getInt();

			if (KaalutudKeskhinne < 0 || KaalutudKeskhinne > 5) {
				System.out.print("Viga, palun sisestage number uuesti");
				continue;
			}

			System.out.print("Sisesta oma l�put�� hinne ");
			while (true) {
				L6put22 = TextIO.getInt();

				if (L6put22 < 0 || L6put22 > 5) {
					System.out.print("Viga, palun sisestage number uuesti");
					continue;
				}

				if (KaalutudKeskhinne >= 4.5 && L6put22 == 5)
					System.err.println("Jah, saad cum laude diplomile!");
				else
					System.out.println("Ei saa!");
				break;

			}
			break;
		}
	}

}
