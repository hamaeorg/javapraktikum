package praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class TekstiNumbriKeskmine {
	
	/* Kirjutada programm, mis loeb tekstifailist hulga numbreid 
	 * (iga number on eraldi real), arvutab nende aritmeetilise 
	 * keskmise ja trükib selle ekraanile. 
	 * Vihje: Stringist numbri saab nii: 
	 * double nr = Double.parseDouble(line);
	 */
	public static void main(String[] args) {
		String failinimi = "numbrid.txt";
		
		System.out.println(numbrid(failinimi));
		System.out.println(keskmine(numbrid(failinimi)));
		
	}
	
	public static ArrayList<Double> numbrid(String failinimi){
		ArrayList<Double> read = new ArrayList<Double>();
		
	    String kataloogitee = FailiLugeja.class.getResource(".").getPath();
	    
		File file = new File(kataloogitee + failinimi);
		
		try {

			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;

			while ((rida = in.readLine()) != null) {
				double nr = Double.parseDouble(rida);
				read.add(nr);

			}
		}
		catch (FileNotFoundException e) {
		    System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		return read;
	}
	
	public static double keskmine (ArrayList<Double> list) {
		double summa = 0;
		for(int i = 0; i < list.size(); i++) {
			summa = summa + list.get(i);
		}
		return summa/(list.size());
	}

}
