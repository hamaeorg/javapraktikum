package praktikum14;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

public class FailiLugeja {
	public static void main(String[] args) {
		String failinimi = "kala.txt";
		String[] laused =  list(failinimi);
		sortimine(laused);
		for (int i = 0; i < laused.length; i++) {
			System.out.println(laused[i]);
		}
		
		System.out.println();
		
		System.out.println(loeFail(failinimi));
	}
		
	public static String[] list(String failinimi){   
			
	    // punkt tähistab jooksvat kataloogi
	    String kataloogitee = FailiLugeja.class.getResource(".").getPath();
	    
	    // otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(kataloogitee + failinimi);
		
		String[] read = new String[4];
		try {
		    // avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;
			int i = 0;

			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				read[i] = rida;
				i++;
			}
		}
		catch (FileNotFoundException e) {
		    System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		return read;

	}
	
	public static String[] sortimine(String[] list){
		Arrays.sort(list);
		return list;
	}
	
	

	public static ArrayList<String> loeFail(String failinimi) {
		ArrayList<String> read = new ArrayList<String>();
	
		// punkt tähistab jooksvat kataloogi
	    String kataloogitee = FailiLugeja.class.getResource(".").getPath();
	    
	    // otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(kataloogitee + failinimi);
		try {
		    // avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;

			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				read.add(rida);

			}
		}
		
		catch (FileNotFoundException e) {
		    System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		return read;

	}
}
