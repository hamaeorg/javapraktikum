package praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class MatrixiLugemine {

	public static void main(String[] args) {

		String failinimi = "matrix.txt";
		System.out.println(read(failinimi));
		System.out.println();
		int[][] matrix = getMatrix(failinimi);
		int[][] tmatrix = transposer(getMatrix(failinimi));

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.println();
		
		for (int i = 0; i < tmatrix.length; i++) {
			for (int j = 0; j < tmatrix[i].length; j++) {
				System.out.print(tmatrix[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public static int[][] transposer(int[][] maatriks) {
		int[][] tMatrix = new int[maatriks[0].length][maatriks.length];
		for (int i = 0; i < maatriks.length; i++){
			for (int j = 0; j < maatriks[0].length; j++) {
				tMatrix[j][i] = maatriks[i][j];
			}
		}
		return tMatrix;
	}

	public static int[][] getMatrix(String failinimi) {
		ArrayList<String> numbriread = read(failinimi);
		int[][] matrix = new int[numbriread.size()][numbriread.size()];

		for (int i = 0; i < numbriread.size(); i++) {
			String ajutine = numbriread.get(i);
			
			int e = 0;
			for (int j = 0; j < ajutine.length(); j++) {
				if (ajutine.charAt(j) != ' ') {
					int element = Character.getNumericValue(ajutine.charAt(j));
					matrix[i][e] = element;
					e++;
				}
			}
		}
		return matrix;
	}

	public static ArrayList<String> read(String failinimi) {
		ArrayList<String> read = new ArrayList<String>();

		String kataloogitee = FailiLugeja.class.getResource(".").getPath();

		File file = new File(kataloogitee + failinimi);

		try {

			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;

			while ((rida = in.readLine()) != null) {
				read.add(rida);

			}
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error, mingi muu error: " + e.getMessage());
		}
		return read;
	}

}
