package praktikum13;

public class MatrixElemetsSum {

	public static void main(String[] args) {
		
		int[][] maatriks = {{1, 2, 3, 4 }, {4, 6, 3, 8 }, {5, 3, 8, 2}};
		
		for (int i = 0; i < maatriks.length; i++) {
			System.out.println(ridadeSummad(maatriks)[i]);
		}


	}
	
	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summaKokku = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			int summa = 0;
			for (int j = 0; j < maatriks[i].length; j++) {
				summa = summa + maatriks[i][j];
			}
			summaKokku[i] = summa;
		}
		return summaKokku;
	}
}
