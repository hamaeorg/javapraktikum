package praktikum13;

public class KorvalDiagonaaliSumma {

	public static void main(String[] args) {
		
		int[][] maatriks = {{1, 2, 3, 4}, {4, 6, 3, 8}, {5, 3, 8, 2}, {1, 2, 1, 2}};
		System.out.println(korvalDiagonaaliSumma(maatriks));

	}
	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			summa = summa + maatriks[i][((maatriks[i].length) - 1) - i];
		}
		return summa;
		
	}

}
