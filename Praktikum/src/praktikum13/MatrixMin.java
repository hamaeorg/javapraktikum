package praktikum13;

public class MatrixMin {

	public static void main(String[] args) {
		
		int[][] maatriks = {{1, 2, 3, 4 }, {4, 6, 3, 8 }, {5, 3, 9, 2}};
		
		System.out.println(miinimum(maatriks));

	}
	
	public static int miinimum(int[][] maatriks) {
		int[] min = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			min [i] = massiivimin(maatriks[i]);
		}
		return massiivimin(min);
	}
	
	public static int massiivimin(int[] massiiv) {
		int min = Integer.MAX_VALUE;
		for (int arv : massiiv) {
			if (arv < min) {
				min = arv;
			}
		}
		return min;
	}

}
