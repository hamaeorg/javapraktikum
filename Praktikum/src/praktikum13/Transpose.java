package praktikum13;

public class Transpose {

	public static void main(String[] args) {
		
		int[][] maatriks = {{1, 2, 3, 4 }, {4, 6, 3, 8 }, {5, 3, 9, 2}};
		
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				System.out.print(maatriks[i][j] + " ");
			}
			System.out.println(" ");
		}
		
		System.out.println();
		
		int[][] trasponeeritud = transponeeri(maatriks);
		
		for (int i = 0; i < trasponeeritud.length; i++) {
			for (int j = 0; j < trasponeeritud[i].length; j++) {
				System.out.print(trasponeeritud[i][j] + " ");
			}
			System.out.println(" ");
		}

	}
	
	public static int[][] transponeeri(int[][] maatriks) {
		int[][] tMatrix = new int[maatriks[0].length][maatriks.length];
		for (int i = 0; i < maatriks.length; i++){
			for (int j = 0; j < maatriks[0].length; j++) {
				tMatrix[j][i] = maatriks[i][j];
			}
		}
		return tMatrix;
	}

}
