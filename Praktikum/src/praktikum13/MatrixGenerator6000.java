package praktikum13;

public class MatrixGenerator6000 {

	public static void main(String[] args) {
		
		int[][] print = kahegaJaakMaatriks(5, 5
				);
		
		for (int i = 0; i < print.length; i++) {
			for (int j = 0; j < print[i].length; j++) {
				System.out.print(print[i][j] + " ");
			}
			System.out.println(" ");
		}


	}
	
	/* Meetod, mis genereerib parameetritena etteantud 
	 * suurusega maatriksi, kus iga element on rea ja veeruindeksi 
	 * summa kahega jagamise jääk. Indeksid algavad nullist. 
	 */
	public static int[][] kahegaJaakMaatriks(int ridu, int veerge) {
		int[][] tulemus = new int[ridu][veerge];
		for (int i = 0; i < ridu; i ++) {
			for (int j = 0; j < veerge; j++) {
				tulemus[i][j] = ((i + j) % 2);
			}
		}
		return tulemus;
	}

}
