package praktikum13;

public class RowMaxElement {

	public static void main(String[] args) {
		
		int[][] maatriks = {{1, 2, 3, 4 }, {4, 6, 3, 8 }, {5, 3, 9, 2}};
		
		for (int i = 0; i < maatriks.length; i++) {
			System.out.println(ridadeMaksimumid(maatriks)[i]);
		}
	}

	
	public static int[] ridadeMaksimumid(int[][] maatriks){
		int[] maxElemendid = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			int max = Integer.MIN_VALUE;
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maatriks[i][j] > max) {
					max = maatriks[i][j];
				}
			}
			maxElemendid[i] = max;
		}
		return maxElemendid;
	}

}


