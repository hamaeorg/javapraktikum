package praktikum13;

public class PrintMassiiv {

	public static void main(String[] args) {
		
		int[] massiiv = {1, 3, 5, 2, 7, 9, 10, 23, 4};
		tryki(massiiv);

	}
	
	public static void tryki(int[] massiiv) {
		for (int arv : massiiv) {
			System.out.print(arv + " ");
		}		
	}

}
