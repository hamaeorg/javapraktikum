package praktikum07;

import lib.TextIO;

public class Meetodid7 {
	
	public static int randomNumber ( int min, int max){
		int vahemik = max - min + 1;
		return min + (int) (Math.random() * vahemik);
		
	}
	
	public static int panus (String kysimus, int min, int max, int maxmax) {
		System.out.println(kysimus);
		int panus;
		while (true) {
			panus = TextIO.getInt();
			if (panus < min) {
				System.out.println("Pakutud panus on alla min panuse, palun sisesta uus panus");
				continue;
			}
			if (panus > max) {
				System.out.println("Pakutud panus on üle max panuse, palun sisesta uus panus");
				continue;
			}
			if (panus > maxmax) {
				System.out.println("Sul ei ole nii palju raha, palun sisesta uus panus");
				continue;
			} else {
				System.out.println("Panustasite " + panus + " raha");
				break;
			}
		}
		return panus;
	}

}
