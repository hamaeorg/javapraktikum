package praktikum07;

import lib.TextIO;

public class NumberGame {
	public static void main(String[] args) {
		
		int cpuNumber = Meetodid7.randomNumber(1, 100);;

		System.out.println("Arvuti on valinud ühe arvu 1-100. Arva ära mis arvuga on tegemist");
		
		while (true){
			int userNumber = TextIO.getInt();
			if (userNumber > cpuNumber){
				System.out.println("Õige number on väiksem. Proovi uuesti");
				continue;
				}			
			if (userNumber < cpuNumber){
				System.out.println("Õige number on suurem. Proovi uuesti");
				continue;
				}
			if (userNumber == cpuNumber){
				System.out.println("Palju õnne! pakkusid õige numbri.");
				break;		
				}
			else {
				System.out.println("Sisestatud number ei kuul etteantud vahemikku. Proovi uuesti");
				continue;
				}
			
		}
		
	}

}
