package praktikum07;

import lib.TextIO;

public class LiiskMassiiviga {
	
	/* Kirjutada liisu heitmise programm. Programm kysib 10 nime, 
	 * valib neist suvalise nime (randomiga) ja tr�kib v�lja. 
	 * T�iendus: K�sida esimese asjana inimeste arv. */
	
	public static void main(String[] args) {
		System.out.println("Mitu inimest osaleb?");
		int userInput = TextIO.getlnInt();
		
		int[] nameList = new int[(userInput)];
		
		for (int i = 0; i < userInput; i++) {
			nameList[i] = i + 1;
		}
		
		int output = Meetodid7.randomNumber(1, userInput); 
		System.out.println("valituks osutus inimene nr " + nameList[output]); 
	}
}
