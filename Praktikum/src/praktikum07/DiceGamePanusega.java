package praktikum07;

import java.util.Dictionary;

import lib.TextIO;
import praktikum06.Meetodid;

public class DiceGamePanusega {

	public static void main(String[] args) {
		
			int userNumber;
			int userMoney = 100;
			int userBet;
			
			System.out.println("T4ringum�ng panustega");		
			
			while (true) {
				System.out.println("Sul on  " + userMoney + " raha. Max panus on 25 ja min panus on 1");
				userBet  = Meetodid7.panus("Palun sisesta oma panus", 1, 25, userMoney);
				userMoney = userMoney - userBet;

				System.out.println("Vali, mis silmade arvule panustad. Selleks sisesta number 1 - 6! ");
				int DiceNumber = Meetodid7.randomNumber(1, 6);

				while (true) {
					userNumber = TextIO.getInt();
					System.out.println("6ge vastus oli " + DiceNumber);
					if (userNumber < 0 && userMoney > 1) {
						System.out.println("Sisestasid lubamatu numberi, palun vali uuesti");
						continue;
					}
					if (DiceNumber == userNumber) {
						System.out.println("Arvasid Ara!\n");
						userMoney = userMoney + (userBet * 6);
						break;
					}
					else {
						System.out.println("Arvasid valesti!\n");			
						break;
					}
				} 
				if (userMoney <= 0) {
					System.out.println("Sul on raha otsas! M2ng on l2bi!");
					break;
					
				}
			}		
		}
	}

