package praktikum07;

import lib.TextIO;
import praktikum06.Meetodid;

public class KullJaKiriPanusega {

	public static void main(String[] args) {
		
			int userNumber;
			boolean otsus;
			int userMoney = 100;
			int userBet;
			
			System.out.println("Kull ja Kiri");		
			
			while (true) {
				System.out.println("Sul on  " + userMoney + " raha. Max panus on 25 ja min panus on 1");
				userBet  = Meetodid7.panus("Palun sisesta oma panus", 1, 25, userMoney);
				userMoney = userMoney - userBet;

				System.out.println("Sisesta kull (0) või kiri (1)");
				int myndiVise = Meetodid7.randomNumber(0, 1);

				while (true) {
					userNumber = TextIO.getInt();
					if (userNumber < 0 && userMoney > 1) {
						System.out.println("Sisestasid lubamatu numberi, palun vali uuesti");
						continue;
					}
					if (myndiVise == userNumber) {
						System.out.println("Arvasid Ara!");
						userMoney = userMoney + (userBet * 2);
						break;
					}
					else {
						System.out.println("Arvasid valesti!");			
						break;
					}
				} 
				if (userMoney <= 0) {
					System.out.println("Sul on raha otsas! Mäng on läbi!");
					break;
					
				}
			}		
		}
	}

