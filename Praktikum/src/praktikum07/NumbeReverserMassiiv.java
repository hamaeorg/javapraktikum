package praktikum07;

import lib.TextIO;

public class NumbeReverserMassiiv {	
	
		/* Kirjutada programm, mis k�sib kasutajalt 10 
		 * arvu ning tr�kib nad seej�rel vastupidises 
		 * j�rjekorras ekraanile. */
		
		public static void main(String[] args) {
			
			
			System.out.println("Sisesta 10 arvu ja eralda need t�hikutega");
			String userInput = TextIO.getlnString();
			String[] numberList = userInput.split(" ");
			
			for (int i = numberList.length ; i > 0 ; i--) {
				System.out.print(numberList[i - 1] + " ");
			}


	}
	
}
