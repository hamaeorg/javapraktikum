package praktikum10;

import java.util.ArrayList;

import lib.TextIO;

public class Tulpdiagramm {

	public static void main(String[] args) {
		
		ArrayList<Integer> numbrid = new ArrayList<>();
		double kordaja = 1;
		double max = 10;
		
		while (true){
			System.out.println("Sisesta positiivne arv. Lõpetamiseks sisest arv 0.");
			int userInput = TextIO.getlnInt();
			if(userInput == 0) {
				break;
			}else if (userInput < 0) {
				System.out.println("Negatiivne arv ei sobi.");
				continue;
			}else if (userInput > max) {
				kordaja = max / userInput;
				numbrid.add(userInput);
			}else {
				numbrid.add(userInput);
			}
		}

		for (int i = 0; i < numbrid.size(); i++) {
			System.out.printf("%2d %s", (i + 1), (genereeriIksid(numbrid.get(i), kordaja)));
			System.out.println();
		}
	}
	
	public static String genereeriIksid(int nr, double kordaja) {
		String iksid = "";
		for (int i = 0; i < nr * kordaja; i++) {
			iksid = iksid + "x";
		}
		return iksid;
	}
}
