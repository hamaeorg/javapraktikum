package praktikum10;

public class MassiiviMatrixiMaxNumber {
	
	public static void main(String[] args) {
		
		int[][] neo = {{1, 3, 6, 7}, {2, 3, 3, 1}, {17, 4, 5, 0}, {-20, 13, 16, 17}};
		
		System.out.println(topeltmax(neo));	
		
	}
	public static int topeltmax(int massiiv[][]) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < massiiv.length; i++) {
			int ajut =  MassiiviSuurimNumber.suurim(massiiv[i]);
			if (max  < ajut) {
				max = ajut;
			}	
		}
		return max;
	}
}
