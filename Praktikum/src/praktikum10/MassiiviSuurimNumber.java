package praktikum10;

public class MassiiviSuurimNumber {
	
	public static void main(String[] args) {
		
		int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3};
		
		System.out.println(suurim(massiiv));		
		
	}
	
	public static int suurim(int massiiv[]) {
		int max = -Integer.MIN_VALUE;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > max) {
				max = massiiv[i];
			}					
		}
		return max;
	}

}
