package praktikum10;

public class KT1 {

	public static void main(String[] args) {

		int[] numbrid = { -10, 5, -5, 15 };

		long test = -0xaL;

		double[] keskmine = { -200, 0.5, 4, 10, 50 };

		int a = 9;

		int b = a++ / 5;

		short f = -0xa;

		System.out.println(valiVahemik(10));

		// System.out.println(absvSumma(numbrid));
	}

	public static double kuupideSumma(double a, double b) {
		return Math.pow(a, 3) + Math.pow(b, 3);
		// return Math.abs(a, 2) + Math.abs(b, 2); - ruudu puhul
	}

	public static boolean negPaaritu(int n) {
		return n % 2 != 0 && n < 0;
	}

	public static int nullideArv(int[] m) {
		int nullid = 0;
		for (int i = 0; i < m.length; i++) {
			if (m[i] == 0) {
				nullid++;
			}
		}
		return nullid;
	}

	public static int allaKeskmise(double[] d) {

		double summa = 0;
		for (int i = 0; i < d.length; i++) {
			summa = summa + d[i];
		}
		double keskmine = summa / d.length;
		int arvud = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i] < keskmine) {
				arvud++;
			}
		}
		return arvud;

	}

	public static boolean pos2k(int n) {
		return n >= 1 && n <= 100;
	}

	public static int absvSumma(int[] m) {
		int summa = 0;
		for (int i = 0; i < m.length; i++) {
			summa = summa + Math.abs(m[i]);
		}
		return summa;
	}

	public static int negSumma(int[] m) {
		int summa = 0;
		for (int i = 0; i < m.length; i++) {
			if (m[i] < 0) {
				summa = summa + m[i];
			}
		}
		return summa;
	}

	public static int valiVahemik(int m) {
		if (m < 10) {
			return 0;
		} else if (m > 35) {
			return 2;
		} else {
			return 1;
		}
	}
}
