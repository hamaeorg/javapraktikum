package praktikum10;

public class TestKT {

	public static void main(String[] args) {

		// double[] test = { 1.5, 2.0, 2.5, 3 };
		// System.out.println(keskmisestParemaid(test));

		// int[] skoor = { -2, -1, -1, 0 };
		// System.out.println(score(skoor));

		int[][] veeruvark = { { 1, 3 }, { 6, 2, 3, 4 }, { 2, 8, 4 }, { 4 } };
		int[] test4 = veeruMaxid(veeruvark);
		for (int i = 0; i < test4.length; i++) {
			System.out.println(test4[i]);
		}

		
		//int[][] matrix = yhik(5); for (int j = 0; j < 5; j++) {
		//System.out.print(matrix[j][i]); } System.out.println(); 
		//}
		 
	}

	/*
	 * Leiab massiivi elementide aritmeetilise keskmise ning tagastab elementide
	 * arvu, mis on aritmeetilisest keskmisest suuremad.
	 */
	public static int keskmisestParemaid(double[] d) {
		double kokku = 0;
		for (int i = 0; i < d.length; i++) {
			kokku = kokku + d[i];
		}
		double keskmine = kokku / d.length;
		int luger = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i] > keskmine) {
				luger = luger + 1;
			}
		}
		return luger;
	}

	/*
	 * genereerib massiiviga maatrikis, mille pikkuseks ja laiuseks on sisend n.
	 * maatriks koosneb nullidest ja peadiagonaal �htedest
	 */
	public static int[][] yhik(int n) {
		int[][] maatriks = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == j) {
					maatriks[i][j] = 1;
				} else {
					maatriks[i][j] = 0;
				}
			}
		}
		return maatriks;
	}

	/*
	 * Liidab kokku k�ik massiivi numbrid, v�lja arvatud kaks k�ige v�iksemat
	 * numbrit
	 */
	public static int score(int[] points) {
		;
		int score = 0;
		int min1 = Integer.MAX_VALUE;
		int min2 = Integer.MAX_VALUE;
		int min1Asukoht = 0;
		int min2Asukoht = 0;

		for (int i = 0; i < points.length; i++) {
			if (points[i] < min1) {
				min1 = points[i];
				min1Asukoht = i;

			}
		}

		for (int i = 0; i < points.length; i++) {
			if (i != min1Asukoht && points[i] < min2) {
				min2 = points[i];
				min2Asukoht = i;
			}
		}

		for (int i = 0; i < points.length; i++) {
			if (i != min1Asukoht && i != min2Asukoht) {
				score = score + points[i];
			}
		}
		return score;
	}

	/*
	 * Leiab kahem��tmelises massiivis maatriks iga veeru suurimad v��rtused ja
	 * tagasatab iga veeru suurimad v��rtused �hem��tmelise massiivina
	 */
	public static int[] veeruMaxid(int[][] m) {
		int pikimRida = Integer.MIN_VALUE;
		for (int i = 0; i < m.length; i++) {
			if (m[i].length > pikimRida) {
				pikimRida = m[i].length;
			}
		}
		int[] veeruMaxid = new int[pikimRida];

		for (int i = 0; i < pikimRida; i++) {

			int max = Integer.MIN_VALUE;
			for (int j = 0; j < m.length; j++) {
				if (m[j].length > i) {
					if (m[j][i] > max) {
						max = m[j][i];
					}
				} else {
					continue;
				}
			}
			veeruMaxid[i] = max;
		}
		return veeruMaxid;
	}

}
