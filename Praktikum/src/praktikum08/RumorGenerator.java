package praktikum08;

import praktikum07.Meetodid7;

public class RumorGenerator {
	
	/* Kirjutada kuulujutugeneraator. Kolm ühepikkust massiivi: 
	 * Esimeses naisenimed, teises mehenimed, kolmandas tegusõnad. 
	 * Programm võtab igast massiivist �?HE suvalise elemendi ja 
	 * kombineerib nendest lause. Vihje: Massivist suvalise elemendi 
	 * valimiseks arvuta random number vahemikus 0 kuni massiivi 
	 * pikkus (random * length).
	 */
	
	public static void main(String[] args) {
		
		String naiseNimed[] = {"Maimu", "Silvi", "Viive", "Malle", "Tiiu", "Leeni", 
				"Krõõt", "Kai", "Veera", "Irina"};
		String meheNimed[] = {"Toivot", "Jaani", "�?lot", "Manivaldi", "Antsu", "Jaanust", 
				"Viktorit","Lembitut", "Ilmarit", "Atsi"};
		String tegevus[] = {"vihkab", "pussitas", "petab", "tulistas", "peksab", 
				"jälitab", "kiusab", "ahistab", "röövis", "lõi jalaga"};
		
		
		
		for (int i = 0; i < tegevus.length; i++) {
			System.out.print(naiseNimed[Meetodid7.randomNumber(0, 9)] + " ");
			System.out.print(tegevus[Meetodid7.randomNumber(0, 9)] + " ");
			System.out.println(meheNimed[Meetodid7.randomNumber(0, 9)]);
		}
		
						
	}

}
