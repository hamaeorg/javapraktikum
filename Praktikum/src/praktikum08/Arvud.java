package praktikum08;

public class Arvud {
	
	public static void main(String[] args) {
		
		int arv = 3;
		
		int arvud[] = {2, 34 ,45};
		arvud[1] = 55;
		
		for (int i = 0; i < arvud.length; i++) {
			System.out.println(arvud[i]);
		}
		
		int [] kymmeArvu = new int[10];
		kymmeArvu[12] = 2;
		System.out.println(kymmeArvu[12]);
		
	}

}
