package praktikum08;

import java.util.ArrayList;
import java.util.Collections;
import lib.TextIO;

public class NimeSisestus {
	
	/* Kirjutada programm, mis laseb kasutajal sisestada senikaua nimesid, 
	 * kui kasutaja sisestab nime asemel tühja rea. Seejärel trükkida 
	 * nimed ekraanile tähestikulises järjekorras. Vihje: selle ülesande 
	 * kasutamiseks kasuta ArrayList klassi. Sorteerimiseks kasuta 
	 * Collections.sort() meetodit.
	 */

	public static void main(String[] args) {
		
		System.out.println("Kirjuta nii palju nimesid kui soovid. lõpetamiseks sisesta tühi rida");
				
		ArrayList<String> nimed = new ArrayList<String>();
		
		while (true) {  
			String userInput = TextIO.getlnString();
			if(userInput.equals("")){
				break;
			} else {
				nimed.add(userInput);
				continue;
			}
		}			
		for (String nimi : abc(nimed)) {
		    System.out.println(nimi);
		}
		
	}
	
	
	public static ArrayList<String> abc(ArrayList<String> abc) {
		Collections.sort(abc);
		return abc;
	}
}
