package praktikum08;

public class MassiiviMaxElement {
	
	/* Kirjutada meetod, mis leiab ja tagastab ettantud 
	 * kahemõõtmelise massiivi maksimaalse elemendi. Kirjuta 
	 * kõigepealt meetod, mis leiab ühemõõtmelise massiivi 
	 * maksimaalse elemendi ning kasuta siis seda meetodit teises
	 */
	
	public static void main(String[] args) {
		
		int arvud[] = {1, 23, 6, 8, 2, 4};
		System.out.println(maksimum(arvud));
		
		int arvud2[][] = {{1, 4, 7, 8, 3}, {2, 56, 42, 34, 9}, {1, 2, 3, 4, 5}};
		System.out.println(maksimumMatrix(arvud2));
		
	}
	
	public static int maksimumMatrix(int[][] maatriks) {
			int max = 0;
			for (int i = 0; i < maatriks.length ; i++) {
				int submax = maksimum(maatriks[i]);
				if (submax > max){
					max = submax;
				}
			}
			return max;
		}
	

	public static int maksimum(int[] massiiv) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < massiiv.length; i++){
			if (massiiv[i] > max){
				max = massiiv[i];
			}
		}
		return max;
	}
	
}
