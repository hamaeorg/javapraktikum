package praktikum02;

import lib.TextIO;

public class GroupSize {
	public static void main (String[] args) {
		
		int a;
		int b;
		int c;
		int d;
		
		System.out.println("Mitu inimest on?");
		a = TextIO.getlnInt();
		
		System.out.println("kui suured grupid on vaja moodustada?");
		b = TextIO.getlnInt();

		c = a / b;
		d = a % b;
		
		System.out.println(a + " inimesest saab moodustada " + c + " " + b + "-liikmelist gruppi");
		
		if(d != 0){
			System.out.println("�?le jääb " + d + " üks inimene");
		}
	}
}
