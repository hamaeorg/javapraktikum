package praktikum02;

import lib.TextIO;

public class NameLength {
	public static void main (String[] args){
		
		String nimi;
				
		System.out.println("Siseste oma nimi");
		nimi = TextIO.getln();

		int nimePikkus = nimi.length();
		System.out.println(nimePikkus);		
	}
}
