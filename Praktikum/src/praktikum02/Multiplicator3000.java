package praktikum02;

import java.util.Scanner;

public class Multiplicator3000 {
	public static void main (String[] args) {
		
		int a;
		int b;
		int c;
		
		System.out.println("sisesta esimene number:");
		Scanner vastus = new Scanner(System.in);
		a = vastus.nextInt();

		System.out.println("sisest teine number");
		b = vastus.nextInt();
		vastus.close();
		
		c = a * b;
		
		System.out.println(a + " * " + b + " = " + c);
	}
}
