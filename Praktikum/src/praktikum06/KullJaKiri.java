package praktikum06;


public class KullJaKiri {

	public static void main(String[] args) {

		int kasutajaArvab = Meetodid.kasutajaSisestus("sisesta kull (0) või kiri (1)", 0, 1);
		int myndiVise = (Math.random() > 0.5) ? 0 : 1;

		if (myndiVise == kasutajaArvab) {
			System.out.println("Arvasid ära");
		} else {
			System.out.println("Mööda panid");
		}

	}
}
