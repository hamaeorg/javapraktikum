package praktikum06;

import lib.TextIO;

public class Liisk {
	
	/* Kirjutada liisu t�mbamise programm.K�sida kasutajalt inimeste arv.
	   V�ljastada random number vahemikus 1 kuni arv (kaasaarvatud)
	   NB! Kontrollida, et t��tab �igesti: st. �eldes mitu korda j�rjest 
	   arvuks 3, peab v�imalike vastuste hulgas olema nii �htesid, kahtesid kui kolmi. */
	public static void main(String[] args) {
		System.out.println("Mitu inimest osaleb?");
		int userInput = TextIO.getlnInt();
		
		int output = Meetodid.MitmestArvustRandom(userInput);
		System.out.println("Valituks osutus " + output);
	}
	

}
