package praktikum06;

public class DiceGame {
	
	/*
	  Kirjutada t�ringum�ng: Programm viskab kaks t�ringut m�ngijale ja kaks t�ringut
	  endale (arvutile), arvutab m�lema m�ngija silmade summad ja teatab, kellel oli rohkem.
	 */
	public static void main(String[] args) {
		
		int user = Meetodid.DiceRoll(2);
		int computer = Meetodid.DiceRoll(2);
		
		System.out.println("sinu veeretatud t�ringue summa:" + user);
		System.out.println("Arvuti veeretatud t�ringue summa:" + computer);
		
		if (user > computer){
			System.out.println("Sinu v�it!");
		} else if (user < computer) {
			System.out.println("Arvuti v�itis!");
		} else {
			System.out.println("Viik!");
		}
		
	}
}
