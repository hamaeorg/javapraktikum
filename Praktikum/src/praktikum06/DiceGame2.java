package praktikum06;

public class DiceGame2 {
	/*Kirjutada programm, mis viskab 5 t�ringut ning arvutab silmade summa
	  NB! Lahendus peab olema tehtud ts�kliga. Vihje: ts�klis summa arvutamiseks 
	  tuleb summat sisaldav muutuja enne ts�kli k�ivitamist v��rtustada nulliga 
	  ja ts�kli kehas liita sellele �ks haaval v��rtusi otsa (sum = sum + ...)
	*/
	
	public static void main(String[] args) {
		
		int user = Meetodid.DiceRoll(5);
		int computer = Meetodid.DiceRoll(5);
		
		System.out.println("sinu veeretatud t�ringue summa:" + user);
		System.out.println("Arvuti veeretatud t�ringue summa:" + computer);
		
		if (user > computer){
			System.out.println("Sinu v�it!");
		} else if (user < computer) {
			System.out.println("Arvuti v�itis!");
		} else {
			System.out.println("Viik!");
		}
		
	}

}
