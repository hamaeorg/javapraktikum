
package praktikum06;

import lib.TextIO;

public class Meetodid {
	
	public static int DiceRoll(int number){
		int points = 0;
		for( int i = 0; i < number; i++){
			points = points + MitmestArvustRandom(6);			
		}
		return points;
	}
	
	public static int MitmestArvustRandom (int number) {
		number =  (int) ((Math.random() * number) + 1);
		return number;
	}

	public static int kasutajaSisestus(String kysimus, int min, int max) {
		System.out.println(kysimus);
		return kasutajaSisestus(min, max);
	}
	
	public static int kasutajaSisestus(int min, int max) {
		while (true) {
			System.out.println("Palun sisesta arv vahemikus " + min + " kuni " + max);
			int sisestus = TextIO.getlnInt();
			if (sisestus >= min && sisestus <= max) {
				return sisestus;
			} else {
				System.out.println("See arv pole ette antud vahemikus!?");
			}
		}
	}
	
	public static void main(String[] args) {
		int hinne = kasutajaSisestus(1, 5);
		System.out.println("Kasutaja sisestas: " + hinne);
	}
	
	public static int kuup(int number) {
		return (int) Math.pow(number, 3);
	}	
	
}


