package praktikum11;

public class RekursiivneTaisarvuAste {
	public static void main(String[] args) {
		
		System.out.println(astenda(2, 4));
		
	}
	
	public static int astenda(int a, int b) {
		if (b == 1) {
			return a;
		}
		return a * astenda(a, b-1);
	}

}
