package praktikum11;

public class FibonacciArv {

	public static void main(String[] args) {

		int n = 6;
		
		for (int i = 0; i <= n; i++) {
			System.out.print(fibonacci(i) + ", ");
		}

	}
	
	public static int fibonacci(int n) {
		if(n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		}
		return fibonacci(n-1) + fibonacci(n-2);
			        
	}
	public static int fibonacciTsykkel(int n) {
		int arv = 0;
		for (int i = 0; i < n; i++) {
			arv = arv + (arv -1);
			
		}
		return arv;
	}

}
