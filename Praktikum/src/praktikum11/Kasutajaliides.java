/**
 * Created by hamaeorg on 28.10.2016.
 */
package praktikum11;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Kasutajaliides extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.show();
		Pane paigutus = new Pane();
		Scene stseen = new Scene(paigutus, 1280, 720);
		primaryStage.setScene(stseen);
		stseen.setFill(Color.ALICEBLUE);

		Rectangle sinine = new Rectangle(100, 100, 400, 100);
		sinine.setFill(Color.BLUE);

		Rectangle must = new Rectangle(100, 200, 400, 100);
		must.setFill(Color.BLACK);

		Rectangle valge = new Rectangle(100, 300, 400, 100);
		valge.setFill(Color.WHITE);

		paigutus.getChildren().addAll(sinine, must, valge);

	}
}
