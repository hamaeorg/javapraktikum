package praktikum05;

public class NumbriMatrix {
	public static void main(String[] args) {
		
		int j = 0;

		for (int i = 0; i <= 9; i++) {
			for (int e = 0; e <= 9; e++) {
				int f = e + i;
				if (f < 10)
					System.out.print(f + " ");
				else {					
					System.out.print(f - 10 + " ");	
				}
			}
			System.out.println();
		}
	}

}
