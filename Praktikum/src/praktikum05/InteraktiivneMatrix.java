package praktikum05;

import lib.TextIO;

public class InteraktiivneMatrix {
	public static void main(String[] args) {
		
		int UserInput;
		
		System.out.println("vali matriksi suurus");
		UserInput = TextIO.getInt();
		UserInput = UserInput + 1;

		for (int i = 0; i <= UserInput; i++) {
			if (i == 0 || i == UserInput){
				System.out.print(" ");
				for (int f = 0; f < UserInput; f++)
					System.out.print("- ");
			}
			else{
				for (int e = 0; e <= UserInput; e++) {
				if (e == 0 || e == UserInput)
					System.out.print("| ");
				else if (i == e)
					System.out.print("x ");
				else if (i == UserInput - e)
					System.out.print("x ");

				else
					System.out.print("0 ");
				}
			}			
			System.out.println();
		}
	}

}
