package praktikum05;

public class Matrix {
	public static void main(String[] args) {

		for (int i = 0; i < 7; i++) {
			for (int e = 0; e < 7; e++) {
				if (i == e) {
					System.out.print("1 ");
				} else
					System.out.print("0 ");
			}
			System.out.println();
		}
	}
}
