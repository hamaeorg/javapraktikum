package praktikum01;

import java.util.Scanner;

public class Interest {
   
   private static Scanner answer;

public static void main(String[] args) {
   
       /* Declare the variables. */
   
       double principal;     // The value of the investment.
       double rate;          // The annual interest rate.
       double interest;      // Interest earned in one year.
       double FirstSum;		 // The sum entered by  the user.
       int Years;            // Years entered by the user.
       double FinalPrincipal;
       double FinalInterest;
       
       System.out.println("Vali algne investeeritud summa");      
       answer = new Scanner(System.in);
       String user = answer.next();
       FirstSum = Double.parseDouble(user);
       
       System.out.println("Kirjuta, mitme mitme aasta prognoosi soovid");
       String user2 = answer.next();
       Years = Integer.parseInt(user2);
       FinalPrincipal = FirstSum;
       
       for(int i = 0; i < Years; i++){ 
       
	       principal = FinalPrincipal;
	       rate = 0.0643;
	       interest = principal * rate;       
	       principal = principal + interest;
	       FinalPrincipal = principal;
       }
       
       FinalInterest = FinalPrincipal - FirstSum;       
       
       System.out.print("Algne summa: " + FirstSum + "\n");
       System.out.print("Sinu valitud aastaid: " + Years + "\n\n");
       System.out.print("Tallinna vee dividenditootlus aastas " + FirstSum + " euro pealt: \n");
       System.out.print("Teenitud intress: ");
       System.out.println(FinalInterest);
       System.out.print("Investeeringu väärtus kokku: ");
       System.out.println(FinalPrincipal);
                      
   } // end of main()
      
} // end of class Interest