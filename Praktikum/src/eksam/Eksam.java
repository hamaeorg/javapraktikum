package eksam;

import java.util.Arrays;
import java.util.Collections;

import com.sun.org.apache.xpath.internal.operations.Equals;

public class Eksam {

	public static void main(String[] args) {
		
		int[] m = {-1};
		double[] d = {0.0, 0.0, 0.0};
		String s = "te re.12koll__a.ne";
		int[][] maatriks = {{1, 2, 3, 4 }, {4, 5, 3, 3 }, {5, 3, 4, 2}, {5, 5, 5, 5}};
		
		
		
		//System.out.println(sortByAvg(maatriks));
		
		//System.out.println(asenda(s));
		
		System.out.println(greatestPrimeFactor(99));
		
		/*
		int[] ajut = reaMinid(maatriks);
		for (int i = 0; i < maatriks.length; i++) {	
			System.out.println(ajut[i]);
		}
		*/

		

	}
	
	
	/* Koostage Java-meetod, mis leiab 
	 * etteantud massiivi m positiivsete elementide summa. 
	 */
	public static int posSumma (int[] m){
		int summa = 0;
		for (int i : m) {
			if (i > 0){
				summa = summa + i;
			}
		}
		return summa;
	}
	
	
	/* Koostage Java meetod, mis leiab etteantud reaalarvude 
	 * massiivi d põhjal niisuguste elementide arvu, 
	 * mis on rangelt väiksemad kõigi elementide aritmeetilisest 
	 * keskmisest (aritmeetiline keskmine = summa / elementide_arv). 
	 */	
	public static int allaKeskmise (double[] d){
		double summa = 0;
		for (double i : d){
			summa = summa + i;	
		}
		double keskmine = summa / d.length;
		
		System.out.println(keskmine);
		int allaKeskmise = 0;
		for (double i : d){
			if (i < keskmine){
				allaKeskmise++;
			}
		}
		
		return allaKeskmise;	
	}
	
	
	/* Koostada Java meetod, mis asendab suvalises 
	 * parameetrina etteantud sõnes s kõik mittetähed märgiga '*'.
	 * Lahendus peab kasutama tsüklit.
	 */
	public static String asenda (String s){
		String vastus = "";
		
		for (int i = 0; i < s.length(); i++){
			if ((s.charAt(i) >= 'a' && s.charAt(i)  <= 'z') || (s.charAt(i) >= 'A' && s.charAt(i)  <= 'Z') ){
				vastus += s.charAt(i);			
			}else {
				vastus += '*';
			}	
		}
		return vastus;
	}
	
	
	/* On antud positiivne täisarv n. Kirjutada Java meetod, 
	 * mis leiab n suurima algarvulise jagaja.
	 */
	public static int greatestPrimeFactor (int n){
		
		int start = n / 2;
		int max = 1;
		for (int i = start; i > 0; i --) {
			if ( n % i == 0) {
				max = i;
				break;
			}			
		}
		return max;
	}
	
	
	/* Koostage Java meetod etteantud täisarvumaatriksi m reamiinimumide massiivi leidmiseks 
	 * (massiivi i-s element on maatriksi i-nda rea vähima elemendi väärtus). 
	 * Read võivad olla erineva pikkusega. 
	 */
	public static int[] reaMinid (int[][] m){
		int[] reaMinid = new int[m.length];
		for (int i = 0; i < m.length; i++){
			int min = Integer.MAX_VALUE;
			for (int j = 0; j < m[i].length; j++){
				if (m[i][j] < min){
					min = m[i][j];
				}
			reaMinid[i] = min;
			}
		}
		return reaMinid;
	}
	
	
	/* On antud hinnete maatriks (int[][] g), milles on iga üliõpilase jaoks üks rida, 
	 * mille elementideks on selle üliõpilase hinded (skaalal 0 kuni 5). 
	 * Koostada Java meetod üliõpilaste pingerea moodustamiseks, 
	 * mis tagastaks reanumbrite massiivi (kõrgeima keskhindega reast allapoole, 
	 * võrdsete korral jääb ettepoole see rida, mille number on väiksem). 
	 */
	public static int[] sortByAvg (int[][] g){
		int[] pingerida = new int[g.length];
		double[] keskhinded = new double[g.length];
		for (int i = 0; i < g.length; i++){	
			double summa = 0;
			for (int j = 0; j < g[i].length; j++){
					summa = summa + g[i][j];	
			}
			double keskmine = summa / g[i].length;
			keskhinded[i] = keskmine;
		}
		double[] sorditudKeskhinded = new double[g.length];
		System.arraycopy( keskhinded, 0, sorditudKeskhinded, 0, keskhinded.length );
		
		Arrays.sort(sorditudKeskhinded);
		double[] maxSorditudKeskhinded = new double[g.length];
		
		for (int i = 0, j = sorditudKeskhinded.length - 1; j >= 0; i++, j --) {
			maxSorditudKeskhinded[i] = sorditudKeskhinded[j];
		}

		for (int i = 0; i < keskhinded.length; i++){
			for (int j = 0; j < keskhinded.length; j++) {
				if (maxSorditudKeskhinded[i] == keskhinded[j]){
					pingerida[i] = j + 1;
				}	
			}
		}
		System.out.println();
		for (int i = 0; i < pingerida.length; i++){
			System.out.println(pingerida[i]);
		}
		
		return pingerida;
	}
}
