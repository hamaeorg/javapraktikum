package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Ramp extends Applet{
	
	@Override
	public void paint(Graphics g) {
		
		int h = getHeight();
		int w = getWidth();
		
		for (int y = 1; y < h; y++) {
			double c = 1 -((double) y / h);
			int a =  (int) (c * 200);
			Color color = new Color (a, 0, 255);
			g.setColor(color);
			g.drawLine(0, y, w, y);
		}
	}

}
