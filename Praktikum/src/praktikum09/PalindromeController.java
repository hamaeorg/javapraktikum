package praktikum09;

import lib.TextIO;

public class PalindromeController {
	public static void main(String[] args) {
		
		System.out.println("sisesta s�na");
		String userInput = TextIO.getlnString();
		
		if (userInput.equals(reverser(userInput))) {
			System.out.println("Tegemist on palindroomiga");
		} else {
			System.out.println("Tegemist ei ole palindroomiga");
		}
		
	}
	
	public static String reverser (String sona) {
		String reversed = "";
		for (int i = sona.length(); i > 0; i--) {
			reversed = reversed + sona.charAt(i-1); 
		}
		return reversed;	
	}

}
