package praktikum09;

import java.util.ArrayList;

import lib.TextIO;

public class Inimesed {

	public static void main(String[] args) {

		ArrayList<Inimene> inimesed = new ArrayList<>();
		String nimi;
		int vanus;

		while (true) {

			System.out.println("Sisesta nimi");
			nimi = TextIO.getlnString();
			
			if (nimi.equals("")) {
				break;
			}

			System.out.println("Sisesta vanus");
			vanus = TextIO.getlnInt();
			
				inimesed.add(new Inimene(nimi, vanus));
				//continue;
			
		}

		for (Inimene inimene : inimesed) {
			// Java kutsub v�lja Inimene klassi toString() meetodi
			System.out.println(inimene);
		}

	}

}
