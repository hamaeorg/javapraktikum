package praktikum09;

import java.util.Arrays;

public class MassiiviMediaan {

	/*
	 * Kirjuta meetod, mis arvutab kahemõõtmelise massiivi rea elementide
	 * mediaani ning tagastab suurima mediaaniga rea elemendid.
	 */

	public static void main(String[] args) {

		int[][] massiiv = { { 1, 2, 3, 4 }, { 2, 4, 6, 7, 19 }, { 20, 2, 6, 12 } };

		int[] result = maxMedianRow(massiiv);
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + " ");
		}
	}

	public static int[] maxMedianRow(int[][] matrix) {
		double max = Double.MIN_VALUE;
		int row = 0;
		for (int i = 0; i < matrix.length; i++) {
			double median = getMedian(matrix[i]);
			if (median > max) {
				max = median;
				row = i;
			}
		}
		return matrix[row];
	}

	public static double getMedian(int[] array) {
		int[] temp = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			temp[i] = array[i];
		}

		Arrays.sort(temp);
		double median;
		if ((temp.length % 2) == 0) {
			median = (temp[(temp.length / 2)] + temp[((temp.length / 2) - 1)]) / 2.0;

		} else {
			median = temp[((temp.length) - 1) / 2];
		}
		return median;
	}

}
