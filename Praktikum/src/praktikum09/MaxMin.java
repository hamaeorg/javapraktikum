package praktikum09;

public class MaxMin {

	/*
	 * Kirjuta meetod, mis tagastab kahemõõtmelise massiivi suurima ja väikseima
	 * elemendi vahe
	 */
	public static void main(String[] args) {

		int[][] massiiv = { { 6, 7, 8, 11 }, { 9, 4, 6, 7, 19 }, { 20, 9, 6, 12 } };

		System.out.println(maxAndMinDifference(massiiv));
	}

	public static int maxAndMinDifference(int[][] matrix) {

		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {

				if (matrix[i][j] > max) {
					max = matrix[i][j];
				}
				if (matrix[i][j] < min) {
					min = matrix[i][j];
				}
			}
		}
		return max - min;
	}
}
