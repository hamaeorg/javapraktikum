package praktikum09;

public class RandomMassiivid {
	
	public static void main(String[] args) {

		
		int [][] massiiv = generateMatrix(5, 5);
		
		for (int i = 0; i < massiiv.length; i++){
			for (int j = 0; j < massiiv[i].length; j++) {
				System.out.print(massiiv[i][j]);
				System.out.print(" ");
			}
			System.out.println("");
		}
	}
	
	public static int[][] generateMatrix(int width, int heigth) {
		int[][] matrix = new int[width][heigth];
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < heigth; j++) {
				matrix[i][j] = (int) ((Math.random() * 100) + 1);
			}
		}
		return matrix;		
	}
}
