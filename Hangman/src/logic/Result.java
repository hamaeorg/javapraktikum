package logic;

public enum Result {
	VICTORY("V�itsid m�ngu!"),
	LOSS("Kaotasid m�ngu!"),
	TRY_AGAIN("Oled seda t�hte juba pakkunud. Paku uuesti!"),
	RIGHT_GUESS("�ige pakkumine"),
	WRONG_GUESS("Vale pakkumine"),
	EMPTY("");
	
	
	private String message;
	
	private Result(String message){
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
