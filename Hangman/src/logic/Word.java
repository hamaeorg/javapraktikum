package logic;
import java.util.ArrayList;
import java.util.List;

public class Word {

	private List<Letter> letters;

	public Word(String word) {
		letters = new ArrayList<>();
		for (int i = 0; i < word.length(); i++) {
			letters.add(new Letter(word.charAt(i)));
		}
	}

	public boolean guess(String word) {
		if (word.length() > 1) {
			if (!guessWord(word)) {
				return false;
			}
			return true;
		}
		if (!guessLetter(word.charAt(0))) {
			return false;
		}
		return true;
	}

	public boolean guessWord(String word) {
		if (word.length() != letters.size()) {
			return false;
		}
		for (Letter l : letters) {
			if (!l.equals(word.charAt(letters.indexOf(l)))) {
				return false;
			}
		}
		setAllUnhidden();
		return true;
	}

	public boolean guessLetter(char letter) {
		boolean rightLetter = false;
		for (Letter l : letters) {
			if (l.equals(letter)) {
				l.setUnhidden();
				rightLetter = true;
			}
		}
		return rightLetter;
	}

	@Override
	public String toString() {
		String print = "";
		for (Letter l : letters) {
			char letter = l.getValue();
			print += letter + " ";
		}
		return print;
	}

	public void setAllUnhidden() {
		for (Letter l : letters) {
			l.setUnhidden();
		}
	}

	public boolean areAllUnhidden() {
		for (Letter l : letters) {
			if (l.isHidden()) {
				return false;
			}
		}
		return true;
	}

}
