package logic;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class WordReader {

	public static String getRandomWord() {
		List<String> words = readFile("words.txt");
		int random = (int) ((Math.random() * words.size()));
		return words.get(random);
	}

	private static ArrayList<String> readFile(String fileName) {
		ArrayList<String> read = new ArrayList<String>();

		String path = WordReader.class.getResource(".").getPath();

		File file = new File(path + fileName);
		try {

			BufferedReader in = new BufferedReader(new FileReader(file));
			String line;

			while ((line = in.readLine()) != null) {
				read.add(line);

			}
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		return read;

	}

}
