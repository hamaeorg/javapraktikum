package logic;

public class Letter {

	private char value;
	private boolean isHidden = true;

	public Letter(char value) {
		this.value = Character.toUpperCase(value);
	}

	public char getLetter() {
		return value;
	}

	public boolean isHidden() {
		return isHidden;
	}

	public boolean equals(char guess) {
		guess = Character.toUpperCase(guess);
		return value == guess;
	}

	public char getValue() {
		if (isHidden) {
			return '_';
		}
		return value;
	}

	public void setUnhidden() {
		isHidden = false;
	}

}
