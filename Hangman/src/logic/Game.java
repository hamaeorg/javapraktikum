package logic;
import java.util.ArrayList;
import java.util.List;

public class Game {

	private Word word;
	private List<Character> guessedLetters;
	private int fails;

	public Game() {
		this.word = new Word(WordReader.getRandomWord());
		guessedLetters = new ArrayList<>();
		fails = 0;
	}

	public Result guess(String userInput) {
		if (userInput.isEmpty()) {
			return Result.EMPTY;
		} else if (isAlreadyQuessed(userInput)) {
			return Result.TRY_AGAIN;
		} else if (!word.guess(userInput)) {
			addGuessedLettersIfLetter(userInput);
			fails++;
			if (fails > 5) {
				word.setAllUnhidden();
				return Result.LOSS;
			}
			return Result.WRONG_GUESS;
		} else if (word.areAllUnhidden()) {
			return Result.VICTORY;
		}
		addGuessedLettersIfLetter(userInput);
		return Result.RIGHT_GUESS;
	}

	private void addGuessedLettersIfLetter(String userInput) {
		if (userInput.length() == 1) {
			guessedLetters.add(userInput.charAt(0));
		}
	}

	private boolean isAlreadyQuessed(String userInput) {
		return userInput.length() == 1 && guessedLetters.contains(userInput.charAt(0));
	}

	public String printWord() {
		return word.toString();
	}

	public String printGuessedLetters() {
		String temp = "";
		for (char c : guessedLetters) {
			temp += guessedLetters.get(guessedLetters.indexOf(c)) + " ";
		}
		return temp.toUpperCase();
	}

	public String getFails() {
		return fails + "";
	}
}
