package ui;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import logic.Game;
import logic.Result;

public class UI extends Application {

	private Stage window;
	private Scene secondScene;
	private Text hiddenWord;
	private Text guessedLetters;
	private Text fails;
	private Text message;
	private TextField input;
	private Button submit;
	private Button reset;
	private GridPane gridpane;
	private Game game;

	public static void name(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		game = new Game();

		window = primaryStage;
		window.setTitle("Hangman");
		window.setScene(createFirstScene());

		gridpane = createGridPane();
		secondScene = createSecondScene(gridpane);

		hiddenWord = createHiddenWord();
		guessedLetters = createGuessedLetters();
		fails = createFails();
		input = createTextInput();
		message = createMessage();
		submit = createSubmitButton();

		gridpane.getChildren().addAll(submit, createHiddenWordLabel(), hiddenWord, input, createGuessedLettersLabel(),
				guessedLetters, createFailsLabel(), fails, message);

		window.show();

	}

	private Scene createSecondScene(GridPane gridpane) {
		Scene scene = new Scene(gridpane, 500, 500);
		return scene;
	}

	private Text createMessage() {
		Text fails = new Text();
		GridPane.setConstraints(fails, 6, 20);
		GridPane.setColumnSpan(fails, 2);
		return fails;
	}
	

	private Text createFails() {
		Text fails = new Text(game.getFails());
		GridPane.setConstraints(fails, 7, 14);
		return fails;
	}

	private Text createFailsLabel() {
		Text failsLabel = new Text("valepakkumisi: ");
		GridPane.setConstraints(failsLabel, 6, 14);
		return failsLabel;
	}

	private Text createGuessedLetters() {
		Text guessedLetters = new Text(game.printGuessedLetters());
		GridPane.setConstraints(guessedLetters, 7, 12);
		return guessedLetters;
	}

	private Text createGuessedLettersLabel() {
		Text guessedLettersLabel = new Text("pakutud t�hed:");
		GridPane.setConstraints(guessedLettersLabel, 6, 12);
		return guessedLettersLabel;
	}

	private Text createHiddenWord() {
		Text hiddenWord = new Text(game.printWord());
		GridPane.setConstraints(hiddenWord, 7, 10);
		return hiddenWord;
	}

	private Text createHiddenWordLabel() {
		Text hiddenWordLabel = new Text("arvatav s�na:");
		GridPane.setConstraints(hiddenWordLabel, 6, 10);
		return hiddenWordLabel;
	}

	private GridPane createGridPane() {
		GridPane gridpane = new GridPane();
		//gridpane.setGridLinesVisible(true);
		gridpane.setVgap(10);
		gridpane.setHgap(22);
		gridpane.setPadding(new Insets(10, 10, 10, 10));
		return gridpane;
	}

	private Scene createFirstScene() {
		Scene scene = new Scene(createFirstLayout(), 500, 500);
		return scene;
	}

	private StackPane createFirstLayout() {
		StackPane layout = new StackPane();
		layout.getChildren().add(createStartButton());
		return layout;
	}

	private Button createStartButton() {
		Button startButton = new Button("Alusta m�ngu");
		startButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				window.setScene(secondScene);
			}
		});
		return startButton;
	}
	
	private Button createResetButton() {
		Button resetButton = new Button("Alusta uuesti");
		resetButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				game = new Game();
				input.disableProperty().set(false);
				submit.disableProperty().set(false);
				gridpane.getChildren().remove(reset);
				refesh();
				
			}
		});
		GridPane.setConstraints(resetButton, 6, 30);
		GridPane.setHalignment(resetButton, HPos.CENTER);
		return resetButton;
	}


	private Button createSubmitButton() {
		Button button = new Button("Paku");
		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				refesh();
			}
		});
		GridPane.setConstraints(button, 7, 25);
		return button;
	}

	private TextField createTextInput() {
		TextField input = new TextField();
		input.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					refesh();
				}
			}
		});
		GridPane.setConstraints(input, 6, 25);
		return input;
	}

	private void refesh() {
		Result result = game.guess(input.getText());
		message.setText(result.getMessage());
		hiddenWord.setText(game.printWord());
		guessedLetters.setText(game.printGuessedLetters());
		fails.setText(game.getFails());
		input.clear();

		if (result.equals(Result.VICTORY) || result.equals(Result.LOSS)) {
			input.disableProperty().set(true);
			submit.disableProperty().set(true);
			reset = createResetButton();
			gridpane.getChildren().add(reset);
		}
	}
}
